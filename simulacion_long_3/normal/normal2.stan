
data {

    int<lower=1> m; // individuos 
    int<lower=1> n; 
    int<lower=1> p; // variables (fijas)

    int id[n];      // identificador de los individuos

    real t[n];
    real x[n];
    real<lower=0,upper=1> y[n];
} 


transformed data {

    real log_t[n];
    for(i in 1:n) log_t[i]<-log(t[i]);

}


parameters {

    real beta0;
    real beta1;
    real beta2;

    real delta0;
    real delta1;
    real delta2;

    real<lower=0> sigma_x;
    real<lower=0> sigma_z;

    real be[m];
    real de[m];

} 


model {

    real mu[n];
    real sigma[n];    

    beta0~normal(0,10);
    beta1~normal(0,10);
    beta2~normal(0,10);

    delta0~normal(0,10);
    delta1~normal(0,10);
    delta2~normal(0,10);

    sigma_x~normal(0,33);
    sigma_z~normal(0,33);

    for(i in 1:n){

        mu[i]<-inv_logit( be[id[i]]+ beta0+beta1*log_t[i]+beta2*x[i] ) ;

		sigma[i]<-sqrt(exp( de[id[i]]+ delta0+delta1*log_t[i]+delta2*x[i] ));

		lp__<-lp__-(pow(y[i]-mu[i],2)/(2*pow(sigma[i],2)*y[i]*(1-y[i])*pow(mu[i],2)*pow(1-mu[i],2)))-log(sqrt(2*pi()*pow(sigma[i],2)*pow(y[i]*(1-y[i]),3)));

        }

    for(j in 1:m)    be[j]~normal(0,sigma_x);
    for(j in 1:m)    de[j]~normal(0,sigma_z);

}

generated quantities {

    real mu[n];
    real sigma[n];    
    real cpo2[n];
	real dev;
    dev<-0;

	for(i in 1:n) {

        mu[i]<-inv_logit( be[id[i]]+ beta0+beta1*log_t[i]+beta2*x[i] ) ;

		sigma[i]<-sqrt(exp( de[id[i]]+ delta0+delta1*log_t[i]+delta2*x[i] ));

		dev<-dev-2*(-(pow(y[i]-mu[i],2)/(2*pow(sigma[i],2)*y[i]*(1-y[i])*pow(mu[i],2)*pow(1-mu[i],2)))-log(sqrt(2*pi()*pow(sigma[i],2)*pow(y[i]*(1-y[i]),3))));

        cpo2[i]<-1/(exp(-(pow(y[i]-mu[i],2)/(2*pow(sigma[i],2)*y[i]*(1-y[i])*pow(mu[i],2)*pow(1-mu[i],2)))-log(sqrt(2*pi()*pow(sigma[i],2)*pow(y[i]*(1-y[i]),3)))));
	
			}
}


