
data {

    int<lower=1> m; // individuos 
    int<lower=1> n; 
    int<lower=1> p; // variables (fijas)

    int id[n];      // identificador de los individuos

    real t[n];
    real x[n];
    real<lower=0,upper=1> y[n];
} 


transformed data {

    real log_t[n];
    for(i in 1:n) log_t[i]<-log(t[i]);

}


parameters {

    real beta0;
    real beta1;
    real beta2;

    real delta0;

    real<lower=0> sigma_x;

    real be[m];

} 


model {

    real mu[n];
    real sigma[n];    

    beta0~normal(0,1000);
    beta1~normal(0,1000);
    beta2~normal(0,1000);

    delta0~normal(0,1000); 

    sigma_x~inv_gamma(1,1);

    for(i in 1:n){

        mu[i]<-inv_logit( be[id[i]]+ beta0+beta1*log_t[i]+beta2*x[i] ) ;

		sigma[i]<-sqrt(exp( delta0 ));

		lp__<-lp__-(pow(y[i]-mu[i],2)/(2*pow(sigma[i],2)*y[i]*(1-y[i])*pow(mu[i],2)*pow(1-mu[i],2)))-log(sqrt(2*pi()*pow(sigma[i],2)*pow(y[i]*(1-y[i]),3)));

        }

    for(j in 1:m)    be[j]~normal(0,sigma_x);
}


generated quantities {

	real mu[n];
	real sigma[n];
    real cpo2[n];
	real dev;
    dev<-0;

	for(i in 1:n) {

        mu[i]<-inv_logit( be[id[i]]+ beta0+beta1*log_t[i]+beta2*x[i] ) ;

		sigma[i]<-sqrt(exp( delta0 ));

		dev<-dev-2*(-(pow(y[i]-mu[i],2)/(2*pow(sigma[i],2)*y[i]*(1-y[i])*pow(mu[i],2)*pow(1-mu[i],2)))-log(sqrt(2*pi()*pow(sigma[i],2)*pow(y[i]*(1-y[i]),3))));

        cpo2[i]<-1/(exp(-(pow(y[i]-mu[i],2)/(2*pow(sigma[i],2)*y[i]*(1-y[i])*pow(mu[i],2)*pow(1-mu[i],2)))-log(sqrt(2*pi()*pow(sigma[i],2)*pow(y[i]*(1-y[i]),3)))));
	
			}
}


