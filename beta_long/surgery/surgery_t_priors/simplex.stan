
data {

    int<lower=1> m; // individuos 
    int<lower=1> n; // en general es m*(cantidad de veces que se repite cada uno de ellos). Aquí será simplemente el número de lineas de la matriz que contenga el conjunto de datos
    int<lower=1> p; // variables (fijas)

    int id[n];      // identificador de los individuos

    real t[n];
    real x[n];
    real<lower=0,upper=1> y[n];

    vector[4] mu_beta;
    matrix[4, 4] Sigma_beta;

} 


transformed data {

    real log_t[n];
    real log_t2[n];

    for(i in 1:n) log_t[i]<-log(t[i]);

    for(i in 1:n) log_t2[i]<-pow(log(t[i]),2);

}


parameters {

    vector[4] beta;

    real delta;

    real<lower=0> nu_b;

    real<lower=0> sigma_b;

    real be[m];

} 


model {

    real mu[n];
    real phi[n];    

    beta~multi_normal(mu_beta, Sigma_beta);

    delta~student_t(10, 0, 10);

    nu_b~exponential(0.1);

    sigma_b~uniform(0,10);

    for(i in 1:n){

        mu[i]<-inv_logit( be[id[i]]+ beta[1]+beta[2]*log_t[i]+beta[3]*log_t2[i]+beta[4]*x[i] ) ;
    		phi[i]<-sqrt( exp( delta )  );

    		increment_log_prob(-(pow(y[i]-mu[i],2)/(2*pow(phi[i],2)*y[i]*(1-y[i])*pow(mu[i],2)*pow(1-mu[i],2)))-log(sqrt(2*pi()*pow(phi[i],2)*pow(y[i]*(1-y[i]),3))));

        }

    be~student_t(nu_b, 0, sigma_b);

}


generated quantities {

	real mu[n];
	real phi[n];
	real dev;
  dev<-0;

	for(i in 1:n) {

      mu[i]<-inv_logit( be[id[i]]+ beta[1]+beta[2]*log_t[i]+beta[3]*log_t2[i]+beta[4]*x[i] ) ;
      phi[i]<-sqrt( exp( delta )  );

      dev<-dev-2*(-(pow(y[i]-mu[i],2)/(2*pow(phi[i],2)*y[i]*(1-y[i])*pow(mu[i],2)*pow(1-mu[i],2)))-log(sqrt(2*pi()*pow(phi[i],2)*pow(y[i]*(1-y[i]),3))));

			}
}


