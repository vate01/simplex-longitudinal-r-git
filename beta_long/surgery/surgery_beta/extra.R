
# esta función está diseñada para el modelo de cirugías (¡¡BETA!!)
# servirá para compararlo con el que ya está (ya submitted)
DIC<-function(mo, y, id, log_t, log_t2, x)
{

  theta_fixed<-apply( extract( mo, permuted = F), 3, mean )[1:7]
  Davg<-apply( extract( mo, permuted = F), 3, mean )[7]
  theta_random<-apply(extract(mo, permuted = F), 3, mean )[8:38]

  mmu<-inv.logit( theta_random[id] + theta_fixed[1] + theta_fixed[2]*log_t+ theta_fixed[3]*log_t2 + theta_fixed[4]*x  )
  pphi<- exp( theta_fixed[5] ) 

  Dtheta<--2*sum( dbeta( y, mmu*pphi, pphi*(1-mmu), log=TRUE ) )
  pD<-Davg-Dtheta
  DIC<-2*Davg-Dtheta

  sds<-apply( extract( mo, permuted = F), 3, sd )
  means<-apply( extract( mo, permuted = F), 3, mean )
  
  return(list(DIC=DIC,pD=pD,Davg=Davg, Dtheta=Dtheta,theta_fixed=theta_fixed,theta_random=theta_random,mu=mmu,phi=pphi,sds=sds, means=means))

}
