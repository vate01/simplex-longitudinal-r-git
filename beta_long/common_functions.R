
ssimplex<-function(y,mu,sigma)
{
  return(-2*sum((-((y-mu)^2)/(2*(sigma^2)*y*(1-y)*(mu^2)*((1-mu)^2)))-log(sqrt(2*pi*(sigma^2)*((y*(1-y))^3)))))
}

# esta función DIC es exclusiva para los datos de mortalidad
DIC<-function(mo, y, x,id, dist)
{
  if(dist!='simplex' & dist!='beta') stop('comprobar distribución')

  theta_fixed<-apply( extract( mo, permuted = F), 3, mean )[1:5]
  Davg<-apply( extract( mo, permuted = F), 3, mean )[6]
  theta_random<-apply(extract(mo, permuted = F), 3, mean )[7:48]

  mmu<-inv.logit( theta_random[1:21][as.numeric(id)] + theta_fixed[1] + theta_fixed[2]*x  )
  pphi<- exp( theta_random[22:42][as.numeric(id)] + theta_fixed[3] )

  if(dist=='beta')
    Dtheta<--2*sum( dbeta( y, mmu*pphi, pphi*(1-mmu), log=TRUE ) )
  if(dist=='simplex')
    Dtheta<-ssimplex(y, mmu, sqrt(pphi))
  pD<-Davg-Dtheta
  DIC<-2*Davg-Dtheta

  sds<-apply( extract( mo, permuted = F), 3, sd )
  means<-apply( extract( mo, permuted = F), 3, mean )
  
  return(list(DIC=DIC,pD=pD,Davg=Davg, Dtheta=Dtheta,theta_fixed=theta_fixed,theta_random=theta_random,mu=mmu,phi=pphi,sds=sds, means=means))

}
