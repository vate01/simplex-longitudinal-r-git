
########
# Cauchy truncada:

function y=f(x,a,b)
    y=(sqrt(4/((pi^2)*a*b))).*sqrt((1./(( 1+(x/a).^2 ))).*(1./(( 1+(x/b).^2 ))));
endfunction

a=[];
b=1:1:150;
j=0;
for i=b
    j=j+1;
    a(j)=quad(@(x) f(x,2.5,i) ,0,Inf);
end

# [b;a]'

quad(@(x) f(x,2.5,107),0,inf)
quad(@(x) f(x,2.5,24),0,inf)

########

# t truncada:

function y=f(t,a,b)
    y=sqrt((4*gamma((a+1)/2)*gamma((b+1)/2))    /(pi*sqrt(a*b)*gamma(a/2)*gamma(b/2))) * (1+(1/a).*t.^2).^(-((a+1)/4)) .* (1+(1/b).*t.^2).^(-((b+1)/4));
endfunction

a=[];
b=1:1:400;
j=0;
for i=b
    j=j+1;
    a(j)=quad(@(t) f(t,1,i) ,0,Inf);
end

# [b;a]'

# lo mayor encontrado: 0.89228
quad(@(x) f(x,1,4),0,inf)
quad(@(x) f(x,1,341),0,inf)


