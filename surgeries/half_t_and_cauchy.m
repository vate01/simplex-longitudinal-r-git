
# Cauchy truncada:

function y=f(x)
y=(sqrt(4/((pi^2)*2.5*10))).*sqrt((1./(( 1+(x/2.5).^2 ))).*(1./(( 1+(x/10).^2 ))));
endfunction

quad("f",0,Inf)

function y=f(x)
y=(sqrt(4/((pi^2)*2.5*100))).*sqrt((1./(( 1+(x/2.5).^2 ))).*(1./(( 1+(x/100).^2 ))));
endfunction

quad("f",0,Inf)

function y=f(x)
y=(sqrt(4/((pi^2)*10*100))).*sqrt((1./(( 1+(x/10).^2 ))).*(1./(( 1+(x/100).^2 ))));
endfunction

quad("f",0,Inf)


# t truncada:

function y=f(t)
y=sqrt((4*gamma((2+1)/2)*gamma((5+1)/2))    /(pi*sqrt(2*5)*gamma(2/2)*gamma(5/2))) *          (1+(1/2).*t.^2).^(-((2+1)/4)) .* (1+(1/5).*t.^2).^(-((5+1)/4));
endfunction

quad("f",0,Inf)

function y=f(t)
y=sqrt((4*gamma((2+1)/2)*gamma((20+1)/2))/(pi*sqrt(2*20)*gamma(2/2)*gamma(20/2))) * (1+(1/2).*t.^2).^(-((2+1)/4)) .* (1+(1/20).*t.^2).^(-((20+1)/4));
endfunction

quad("f",0,Inf)

function y=f(t)
y=sqrt((4*gamma((5+1)/2)*gamma((20+1)/2))/(pi*sqrt(5*20)*gamma(5/2)*gamma(20/2))) * (1+(1/5).*t.^2).^(-((5+1)/4)) .* (1+(1/20).*t.^2).^(-((20+1)/4));
endfunction

quad("f",0,Inf)

