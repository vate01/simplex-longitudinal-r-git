

### Breve inventario:

* surgeries
    + Carpetas con códigos en stan para los datos de cirugía. Además, dos archivos para octave. Lo que se hace aquí es para la distribución simplex.

* simulacion_long_2
    + Carpetas con códigos en stan para las simulaciones. Lo que se hace aquí es para la distribución simplex.

* simulacion_long_3
    + Carpetas con códigos en stan para las simulaciones. También incluye un archivo .R que contiene la justificación del Hellinger utilizado. Lo que se hace aquí es para la distribución simplex.

* beta_long
    + Contiene carpetas con códigos principalmente en stan para varios ejemplos.  Lo que se hace aquí es para la distribución beta. En particular para los ejemplos de cirugía y Prater (**no utilizado**). También contiene un ejemplo con los datos de Figueroa **que tampoco es utilizado**. Contiene su propio archivo de proyecto en R (beta_long.Rproj). Además, contiene (o debe contener) sus propias .RData de los modelos beta estimados para la cirugía.

Los archivos de proyecto que están fuera de estas carpetas, es decir:

1. simulacion_long_2.Rproj
2. simulacion_long_3.Rproj
3. surgeries.Rproj

Hacen referencia a las tres primeras carpetas relacionadas al principio.

Para hallar la distancia entre las previas, se utilizaron aproximaciones numéricas. Ellas están justificadas en los archivos:

1. surgeries/half_t_and_cauchy.m
2. surgeries/half_t_and_cauchy_2.m
3. simulacion_long_3/searchingH.R


